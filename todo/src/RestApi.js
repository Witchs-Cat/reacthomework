const base = 'https://witchscat.pythonanywhere.com/api/'

export function getTagsAsync(token)
{
    return fetch(base+'tags', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        }
    });
}

export function editMemoAsync(memoId, content, memoTagIds, token)
{   
    return fetch(base+'memo/'+memoId, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        },
        body: JSON.stringify({
            content: content,
            tags: memoTagIds
        })
    });
}

export function delMemoAsync(memoId, token)
{
    return fetch(base+'memo/'+memoId, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        }});
}

export function addMemoAsync(content, token)
{
    return fetch(base+'author/me/memos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        },
        body: JSON.stringify({
            content: content
        })
    });
}

export function getMemosAsync(token)
{
    return fetch(base+'author/me/memos', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        }
    });
}

export function getMeAsync(token)
{
    return getAuthorAsync('me', token);
}

export function getAuthorAsync(authorId, token)
{
    return fetch(base+'author/'+authorId, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Token '+ token,
        }
    });
}

export function loginAsync(username, password)
{   
    return fetch(base+'login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username : username,
            password : password
        })
    });
}

export function signupAsync(username, email, password)
{
    return fetch(base+'signup', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username : username,
            email: email,
            password : password
        })
    });
}