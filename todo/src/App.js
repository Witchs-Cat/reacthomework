import React, { useEffect } from 'react';
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import { useAuthData } from './context';
import Cookies from 'js-cookie'
import './styles/App.css'

function App() {    
    const { data, setData } = useAuthData();
    if (!data.isAuth&&!!Cookies.get('is_auth'))
        setData({isAuth: Cookies.get('is_auth'), token: Cookies.get('token')})

    return (
        <BrowserRouter>
            <AppRouter/>
        </BrowserRouter>
    )
}

export default App;