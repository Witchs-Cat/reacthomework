import React, {createContext, useState, useContext} from 'react'

export const AuthContext = createContext(null);

export default function AuthDataProvider({children}) {
    const [data, setData] = useState({isAuth: false, token: null});    return (
    <AuthContext.Provider value ={{data, setData}}>
        {children}
    </AuthContext.Provider>
  );
}

export const useAuthData = () => useContext(AuthContext);