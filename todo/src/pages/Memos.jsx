import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import PrimaryButton from '../components/UI/PrimaryButton'
import { useAuthData } from '../context'
import HeaderLabel from '../components/HeaderLabel'
import MemosContainer from '../components/MemosContainer'
import { getMeAsync} from '../RestApi'
import Cookies from 'js-cookie'

export default function Memos(){
  const [me, setMe] = useState({});
  const { data, setData } = useAuthData();

  const onLogout = () => {
    setData({isAuth: false, token: null});
    Cookies.remove('is_auth');
    Cookies.remove('token');
  };

  useEffect(() => {
    getMeAsync(data.token)
      .then(response => {
        if (response.ok)
          response.json()
            .then(result => setMe(result));
      })
  }, []);

  return (
    <div>      
      <Header>
        <HeaderLabel>{me.username}</HeaderLabel>
        <PrimaryButton onClick = {onLogout}>Выйти</PrimaryButton>
      </Header>
      <MemosContainer/>
    </div>
  )
};
