import React, { useState } from 'react'
import {Typography } from '@material-ui/core'
import MainContainer from '../components/MainContainer'
import AlertLabel from '../components/AlertLabel'
import CustomForm from '../components/UI/CustomForm'
import CustomInput from '../components/UI/CustomInput'
import FlatButton from '../components/UI/FlatButton'
import { useForm } from "react-hook-form"
import { useAuthData } from '../context'
import { Link } from "react-router-dom";
import {loginAsync} from '../RestApi'
import Cookies from 'js-cookie'


export default function Login() {
  const { register, handleSubmit, formState: {errors} } = useForm({
    mode: 'onBlur'
  });
  const { data, setData } = useAuthData();
  const [ loginError, setLoginError ] = useState(null);

  const onSubmit = (fields) => {
    loginAsync(fields.username, fields.password)
      .then(response => {
        response.json()
          .then(result => {
            if (response.ok){
              setData({isAuth: true, token: result.token});
              Cookies.set('is_auth', true);
              Cookies.set('token', result.token);
            }
            else setLoginError(result.detail??result.non_field_errors[0]);
          })
      })
  };

  return (
    <MainContainer>
      <Typography component ='h2' variant = 'h5'>
        Вход
      </Typography>
      <CustomForm>
        <CustomInput
          {...register('username', {
            required: '*Обязательное поле',
            minLength: {
              value: 5,
              message: '*Минимальная длина 5 символов.'}})}
          id="username"
          type="text"
          label="Имя пользователя"
          name="username"          
          error={!!errors?.username}
          helperText={errors?.username?.message}
        />
        <CustomInput
          {...register('password', {
            required: '*Обязательное поле',
            minLength: {
              value: 5,
              message: '*Минимальная длина 5 символов.'}})}
          id="password"
          type="password"
          label="Пароль"
          name="password"
          error={!!errors?.password}
          helperText={errors?.password?.message}
        />
      </CustomForm>
      <Typography style = {{fontSize:'80%'}}>
        <label>Вы здесь впервые? </label>
        <Link to='/signup'>Регистрация.</Link>
      </Typography>
      <FlatButton onClick={handleSubmit(onSubmit)}>Войти</FlatButton>
      {!!loginError?
        <AlertLabel>{loginError}</AlertLabel>
      : ""}
    </MainContainer>
  )
}

