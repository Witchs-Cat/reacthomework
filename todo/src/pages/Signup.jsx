import { Typography } from '@material-ui/core'
import React, {useState}from 'react'
import MainContainer from '../components/MainContainer'
import CustomForm from '../components/UI/CustomForm'
import CustomInput from '../components/UI/CustomInput'
import FlatButton from '../components/UI/FlatButton'
import { useForm } from "react-hook-form"
import { useAuthData } from '../context'
import { Link } from "react-router-dom";
import { signupAsync } from '../RestApi'
import AlertLabel from '../components/AlertLabel'
import Cookies from 'js-cookie'

export default function Signup() {
  const {register, handleSubmit, formState: {errors}} = useForm({
    mode: 'onBlur'
  });
  const { data, setData } = useAuthData();
  const [ loginError, setLoginError ] = useState(null);

  const onSubmit = (fields) => {
    signupAsync(fields.username, fields.email, fields.password)
      .then(response => {
        response.json()
          .then(result => {
            if (response.ok){
              setData({isAuth: true, token: result.token});
              Cookies.set('is_auth', true);
              Cookies.set('token', result.token);
            }
            else setLoginError(result.detail??result.non_field_errors[0]);
          })
      })
  };

  return (
    <MainContainer>
      <Typography component ='h2' variant = 'h5'>
        Регистрация
      </Typography>
      <CustomForm>
        <CustomInput
          {...register('email', {
            required: '*Обязательное поле',})}
          type="email"
          label="почта"
          name="email"
          error={!!errors?.email}
          helperText={errors?.email?.message}
        />
        <CustomInput
          {...register('username', {
            required: '*Обязательное поле',
            minLength: {
              value: 8,
              message: '*Минимальная длина 8 символов.'}})}
          type="text"
          label="Имя пользователя"
          name="username"
          error={!!errors?.username}
          helperText={errors?.username?.message}
        />
        <CustomInput
          {...register('password', {
            required: '*Обязательное поле',
            minLength: {
              value: 8,
              message: '*Минимальная длина 8 символов.'}})}
          type="password"
          label="Пароль"
          name="password"
          error={!!errors?.password}
          helperText={errors?.password?.message}
        />
      </CustomForm>
      <Typography style= {{fontSize:'80%'}}>
        <label>Уже есть аккаунт? </label>
        <Link to='/login'>Войти.</Link>
      </Typography>
      <FlatButton onClick={handleSubmit(onSubmit)}>Зарегистрироваться</FlatButton>
      {!!loginError?
        <AlertLabel>{loginError}</AlertLabel>
      : ""}
    </MainContainer>
  )
}

