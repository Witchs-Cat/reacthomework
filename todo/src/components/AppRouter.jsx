import React, {useContext} from 'react';
import {Route, Routes} from "react-router-dom";
import {routes, Visibility} from "../router";
import {AuthContext} from "../context";

export default function AppRouter(){
    const {data, setDataValues} = useContext(AuthContext);
    
    let filter = (item, index, arr) => item.visibility === Visibility.public || item.visibility === Visibility.onlyPublic;
    if (data.isAuth)
        filter = (item, index, arr) =>  item.visibility === Visibility.private || item.visibility === Visibility.public;
    
    return (
        <Routes>
            {routes
            .filter(filter)
            .map(route =>
            <Route
                element ={<route.element/>}
                path={route.path}
                key={route.path}
            />
            )}
        </Routes>
    );
};
