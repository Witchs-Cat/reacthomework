import React from 'react'
import  { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(-1),
        textAlign: 'center',
        fontSize: '40px',
        background: '#eeeeee',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
}));

export default function Header({children}) {
    const styles = useStyles();
    return (
    <Typography className = {styles.root} component = 'h1' variant = 'h3'>{children}</Typography>
  );
}
