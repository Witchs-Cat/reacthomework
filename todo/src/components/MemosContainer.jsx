import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Select, MenuItem, Container } from '@material-ui/core';
import { getMemosAsync, addMemoAsync, delMemoAsync, editMemoAsync, getTagsAsync } from '../RestApi'
import { useAuthData } from '../context'
import CustomForm from './UI/CustomForm'
import CustomInput from './UI/CustomInput';
import PrimaryButton from './UI/PrimaryButton';
import { useForm } from "react-hook-form"
import MemoItem from './MemoItem';
import MemosEditModelWindow from './MemosEditModelWindow';

const useStyles = makeStyles((theme) => ({
    '@global':{
      body:{
        background:'#ffffff'
      },
      html:{
        background:'#ffffff'
      }
    },
    root: {
      marginTop: theme.spacing(1),
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '90%',
    },
    list: {
      display: 'flex',
      margin: 0,
      padding: 0,
      listStyle: 'none',
      flexWrap: 'wrap',
      width: '100%',
    }
  }));
  

export default function MemosContainer() {
  const [modalWindowVisible, setVisible] = useState(false)
  const [selectedTag, setSelectedTag] = useState(-1)
  const [editableMemo, setEditableMemo] = useState({})
  const [tags, setTags] = useState([])
  const { data, setData } = useAuthData();
  const [memos, setMemos] = useState([])
  const [textFilter, setTextFilter] = useState('')
  const styles = useStyles();
  
  const { register, handleSubmit, formState: {errors} } = useForm({
    mode: 'onBlur'
  });
  
  const updateMemos = () =>
  {
      getMemosAsync(data.token)
      .then(response => {
        if (response.ok)
          response.json()
            .then(result => {
              console.log(result)
              setMemos(result.memos)});
      })
  }
  
  const onMemoAdd = (formData) =>
  {
    addMemoAsync(formData.memoText, data.token)
      .then(result => {
        if (result.ok)
          updateMemos()
      })
  }
  
  const onMemoDelte = ( memo ) =>
  {
      delMemoAsync(memo.id, data.token)
        .then((response) => {
          if (response.ok)
            updateMemos()
        })
  }

  const onMemoEdit = ( memo ) =>
  {
    editMemoAsync(memo.id, memo.content, memo.tags.map(item => item.id, data.token), data.token)
      .then((response) => {
        if (response.ok)
          updateMemos()
      });
  }

  const editMemo = ( memo ) =>
  {
    setVisible(true);
    setEditableMemo(memo);
  }

  useEffect(() => {
    updateMemos();
    getTagsAsync(data.token)
      .then(response =>{
        if (response.ok)
          response.json()
            .then(result => {
              console.log(result)
              setTags(result)})
      })
  }, []);
  const handleChange = (event) => {
    setSelectedTag(event.target.value);
  };
  

  let textSearch = (item) => true;
  let selectFiler = (item) => true; 

  if (selectedTag >= 0)
    selectFiler  = (item) => item.tags.map(tag => tag.id).includes(selectedTag);
  if (textFilter)
    textSearch = (item) => item.content.includes(textFilter);

  const filter = (item) => selectFiler(item) && textSearch(item);

  return (
    <Container 
      key = 'MainContainer'
      className={styles.root}
      component='div'>
        <MemosEditModelWindow 
          open = {modalWindowVisible}
          setOpen = {setVisible}
          onEdit = {onMemoEdit}
          memo = {editableMemo}
          onClose = {() => setEditableMemo({})}
          tags = {tags}/>
        <CustomForm style = {{
          display:'flex',
          alignItems:'baseline'
        }}>
          <CustomInput 
            {...register('memoText')}
            id="memoText"
            type="text"
            label="Текст заметки"
            name="memoText"        
            error={!!errors?.memoText}
            helperText={errors?.memoText?.message} 
          />
          <PrimaryButton key ='search' onClick={handleSubmit((data) => setTextFilter(data.memoText))}>
            Найти
          </PrimaryButton>
          <PrimaryButton key = 'add' onClick={handleSubmit(onMemoAdd)}>
            Добавить
          </PrimaryButton>
        </CustomForm>
        <Select
          labelId="tags"
          id="tags"
          value={selectedTag}
          label="Теги"
          onChange={handleChange}
        >
          <MenuItem key = 'default' value={-1}>Выберите тег</MenuItem>
          {tags.map(item =>
            <MenuItem key = {item.name} value={item.id}>{item.name}</MenuItem>
          )}   
        </Select>
        <Container key = 'NestedContainer' component = 'div' className={styles.list}>
          {memos
            .filter(filter)
            .map(memo =>
            <MemoItem 
            key = {'mi:'+memo.id}
            memo = {memo}
            onEdit = {editMemo}
            onDelete = {onMemoDelte}/>
          )}
        </Container>
    </Container>
  )
}
