import React from 'react'
import { 
    List, ListItem, 
    IconButton, ListItemText, 
    Checkbox, ListItemIcon, 
    Modal, Typography} from '@material-ui/core'
import MainContainer from './MainContainer'
import CustomForm from './UI/CustomForm'
import PrimaryButton from './UI/PrimaryButton'
import {useForm} from 'react-hook-form'
import CustomInput from './UI/CustomInput'
import { useState } from 'react'

export default function MemosEditModelWindow({open, setOpen, onEdit, onClose, memo, tags}) {
    const [memoTags, setMemoTags] = useState([]) 
    const { register, handleSubmit, formState: {errors} } = useForm({
        mode: 'onBlur',
        defaultValues: {
            content: memo.content
        }
      });

    const handleClick = (value) => {
        if (memoTags.indexOf(value) !== -1)
            setMemoTags(memoTags.filter(item => item !== value))
        else setMemoTags([...memoTags, value])
    }
    const edit = (data) => {
        memo.content = data.content;
        memo.tags = memoTags;
        onEdit(memo);
        setOpen(false);
    }

    return (
        <Modal 
            open={open} 
            onClose={() => console.log('close')}>
            <MainContainer component = 'div'>
                <CustomForm>
                    <Typography component='h3' variant = 'h5'>
                        Id = {memo.id}
                    </Typography>
                    <CustomInput 
                        {...register('content', {
                            required: '*Обязательное поле'})}
                        id="content"
                        type="text"
                        label="Контент"
                        name="content"          
                        error={!!errors?.content}
                        helperText={errors?.content?.content}
                        />

                    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', maxHeight: 40 }}>
                    {tags.map((value) => {
                        const labelId = `checkbox-list-label-${value.id}`;

                        return (
                        <ListItem
                            key={value.name}
                            disablePadding
                        >
                            <IconButton role={undefined} onClick={() => handleClick(value)} dense>
                                <ListItemIcon>
                                    <Checkbox
                                    edge="start"
                                    checked={memoTags.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{ 'aria-labelledby': labelId }}
                                    />
                                </ListItemIcon>
                            </IconButton>
                            <ListItemText id={labelId} primary={value.name} />
                        </ListItem>
                        );
                    })}
                    </List>
                    <PrimaryButton onClick = {() => setOpen(false)}>
                        Закрыть
                    </PrimaryButton>
                    <PrimaryButton onClick = {handleSubmit(edit)}>
                        Сохранить
                    </PrimaryButton>
                </CustomForm>
            </MainContainer>
    </Modal>
    )
}
