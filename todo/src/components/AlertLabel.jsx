import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        color:'red',
        marginTop: theme.spacing(-1),
        fontSize:'80%'
    }
}));

export default function AlertLabel({children, ...props}) {
    const style = useStyles() 
    return (
    <Typography className = {style.root} component = 'label' {...props}>{children}</Typography>
  )
}
