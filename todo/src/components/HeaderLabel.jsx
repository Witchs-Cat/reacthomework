import React from 'react'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(0.5,2,0.5)
    }
}));

export default function HeaderLabel({children, ...props}) {
    const styles = useStyles();
    return (
    <Typography 
        className ={styles.root} 
        component='label' 
        variant='h5'>
            {children}
    </Typography>
  )
}
