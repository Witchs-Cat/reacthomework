import React from 'react'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import { Typography, Container } from '@material-ui/core'

const useListItemStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(1),
        display: 'block',
        width: '250px',
        height: 'fit-content',
        wordBreak: 'break-all',
        borderColor:'#5f6368',
        border:'solid',
        borderRadius: '10px'
    }
  })); 

const useIconStyle = makeStyles((theme) => ({
    root:{
        width: '20px',
        height: '20px'
    }
}))

export default function MemoItem({memo, ...props}) {
    const listItemStyles = useListItemStyles();
    const iconStyle = useIconStyle();
    return (
    <Container className={listItemStyles.root} key = {'ul:'+memo.id}>
        <Typography  key = {'li:'+memo.id}>
            {memo.content}
        </Typography >
        { (memo.tags.length > 0)?
        <Typography  key = {'tg:'+memo.id}>
            ____________________ <br/> 
            Теги: {memo.tags.map(item => item.name).join(", ")}           
        </Typography >:""}
        <IconButton key = {'bte:'+memo.id} onClick = {()=>props.onEdit(memo)}>
            <EditIcon className={iconStyle.root}/>
        </IconButton>
        <IconButton key = {'btd:'+memo.id} onClick = {()=>props.onDelete(memo)}>
            <DeleteIcon className={iconStyle.root}/>
        </IconButton>
  </Container>
  )
}
