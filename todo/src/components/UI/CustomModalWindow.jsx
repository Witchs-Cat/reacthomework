import React from 'react';
import { makeStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        display: 'none',
        background: 'rgba(0,0,0, 0.5)'
    },
    activate:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content:{
        padding: '25px',
        background: 'white',  
        borderRadius: '16px',
        minWidth: '250px'
    }
}));

export default function CustomModalWindow({children, visible, setVisible}){
    const style = useStyles()
    const rootClasses = [style.root]

    if (visible) {
        rootClasses.push(style.active);
    }

    return (
        <Container component='div' className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
            <Container component='div'  className={style.content} onClick={(e) => e.stopPropagation()}>
                {children}
            </Container>
        </Container>
    );
};
