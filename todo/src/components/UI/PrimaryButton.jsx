import React from "react";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(0.5, 1, 0.5),
    maxBlockSize: '30px'
  },
}));

export default function PrimaryButton({ children, ...props }){
  const styles = useStyles();

  return (
    <Button
      type='submit'
      variant='contained'
      color='primary'
      className={styles.root}
      {...props}
    >
      {children}
    </Button>
  );
};
