import React from "react";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(0, 0, 2),
  },
}));

export default function FlatButton({ children, ...props }){
  const styles = useStyles();

  return (
    <Button
      type='submit'
      fullWidth
      variant='contained'
      color='primary'
      className={styles.root}
      {...props}
    >
      {children}
    </Button>
  );
};
