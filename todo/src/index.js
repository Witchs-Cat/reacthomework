import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import AuthDataProvider from './context';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <AuthDataProvider>
      <App />
    </AuthDataProvider>
);

