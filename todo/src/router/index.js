import { Enum } from "../utils/enum"
import Login from "../pages/Login"
import Memos from "../pages/Memos"
import Signup from "../pages/Signup"

export const Visibility = Enum({private: "private", public: "public", onlyPublic: "onlyPublic"})

export const routes = [
    {path: '*', element: Login, exact: true, visibility: Visibility.onlyPublic},
    {path: '*', element: Memos, exact: true, visibility: Visibility.private},
    {path: 'login', element: Login, exact: true, visibility: Visibility.onlyPublic},
    {path: 'signup', element: Signup, exact: true, visibility: Visibility.onlyPublic},
    {path: '/', element: Memos, exact: true, visibility: Visibility.private},
]